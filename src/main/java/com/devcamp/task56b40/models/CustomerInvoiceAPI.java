package com.devcamp.task56b40.models;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b40.Customer;
import com.devcamp.task56b40.Invoice;

@RestController
public class CustomerInvoiceAPI {
    @GetMapping("/invoices")

    public ArrayList<Invoice> getListInvoice() {
        Customer customer1 = new Customer(01, "Hoàng", 10);
        Customer customer2 = new Customer(01, "huy", 10);
        Customer customer3 = new Customer(01, "hugn", 10);

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Invoice invoice1 = new Invoice("HD02", customer1, 10000);
        Invoice invoice2 = new Invoice("HD03", customer2, 20000);
        Invoice invoice3 = new Invoice("HD04", customer3, 30000);

        System.out.println(invoice1);
        System.out.println(invoice2);
        System.out.println(invoice3);

        ArrayList<Invoice> invoices = new ArrayList<>();
        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);

        return invoices;
    }
}
