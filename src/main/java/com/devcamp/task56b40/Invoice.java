package com.devcamp.task56b40;

public class Invoice {
    private String id;
    private Customer customer;
    private double amount;

    public Invoice(String id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }
    public String getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public double getAmount() {
        return amount;
    }
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerId(){
        return customer.getId();
    }
    public String getCustomerName(){
        return customer.getName();
    }
    public int getCustomerDiscount(){
        return customer.getDiscount();
    }

     public double getAmountAfterDiscount(){
        return amount -( amount * getCustomerDiscount()/100 );
    }
    @Override
    public String toString() {
        return String.format("Invoice [id = %s, customer=%s(%s) (discount=%s%%), amount=%s]", 
                            this.id, this.customer.getName(), this.customer.getId(),  this.customer.getDiscount(), this.amount);
    }

    
    
}
