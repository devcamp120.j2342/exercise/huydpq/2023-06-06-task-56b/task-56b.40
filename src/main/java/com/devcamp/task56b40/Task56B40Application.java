package com.devcamp.task56b40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B40Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56B40Application.class, args);
	}

}
